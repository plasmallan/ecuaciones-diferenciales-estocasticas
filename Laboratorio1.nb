(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 14.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     24837,        531]
NotebookOptionsPosition[     24218,        511]
NotebookOutlinePosition[     24627,        527]
CellTagsIndexPosition[     24584,        524]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
   "Definir", " ", "la", " ", "ecuaci\[OAcute]n", " ", "diferencial"}], 
   "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"a", "=", "1"}], ";"}], " ", 
   RowBox[{"(*", 
    RowBox[{"Par\[AAcute]metro", " ", "a"}], "*)"}], "\n", 
   RowBox[{
    RowBox[{"b", "=", "2"}], ";"}], " ", 
   RowBox[{"(*", 
    RowBox[{"Par\[AAcute]metro", " ", "b"}], "*)"}], "\[IndentingNewLine]", 
   "\n", 
   RowBox[{
    RowBox[{"f", "[", "x_", "]"}], ":=", 
    RowBox[{"a", " ", 
     RowBox[{"(", 
      RowBox[{"b", "-", "x"}], ")"}]}]}], " ", 
   RowBox[{"(*", 
    RowBox[{
    "Funci\[OAcute]n", " ", "del", " ", "lado", " ", "derecho", " ", "de", 
     " ", "la", " ", "ecuaci\[OAcute]n", " ", "diferencial"}], "*)"}], "\n", 
   "\[IndentingNewLine]", 
   RowBox[{"(*", 
    RowBox[{"M\[EAcute]todo", " ", "de", " ", "Montecarlo"}], "*)"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"n", "=", "1000"}], ";"}], " ", 
   RowBox[{"(*", 
    RowBox[{"N\[UAcute]mero", " ", "de", " ", "trayectorias"}], "*)"}], "\n", 
   RowBox[{
    RowBox[{"T", "=", "10"}], ";"}], " ", 
   RowBox[{"(*", 
    RowBox[{"Tiempo", " ", "final"}], "*)"}], "\n", 
   RowBox[{
    RowBox[{"dt", "=", "0.01"}], ";"}], " ", 
   RowBox[{"(*", 
    RowBox[{"Paso", " ", "de", " ", "tiempo"}], "*)"}], "\n", 
   RowBox[{
    RowBox[{"X0", "=", 
     RowBox[{"RandomReal", "[", 
      RowBox[{"{", 
       RowBox[{"0", ",", "1"}], "}"}], "]"}]}], ";"}], " ", 
   RowBox[{"(*", 
    RowBox[{"Condici\[OAcute]n", " ", "inicial", " ", "aleatoria"}], "*)"}], 
   "\[IndentingNewLine]", "\n", 
   RowBox[{
    RowBox[{"trayectorias", "=", 
     RowBox[{"Table", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"sol", "=", 
         RowBox[{"NDSolve", "[", 
          RowBox[{
           RowBox[{"{", 
            RowBox[{
             RowBox[{
              RowBox[{
               RowBox[{"X", "'"}], "[", "t", "]"}], "==", 
              RowBox[{"f", "[", 
               RowBox[{"X", "[", "t", "]"}], "]"}]}], ",", 
             RowBox[{
              RowBox[{"X", "[", "0", "]"}], "==", "X0"}]}], "}"}], ",", "X", 
           ",", 
           RowBox[{"{", 
            RowBox[{"t", ",", "0", ",", "T"}], "}"}]}], "]"}]}], ";", 
        "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"X", "[", "t", "]"}], "/.", " ", "sol"}]}], ",", 
       RowBox[{"{", 
        RowBox[{"i", ",", "1", ",", "n"}], "}"}]}], "]"}]}], ";"}], "\n", 
   "\[IndentingNewLine]", 
   RowBox[{"(*", 
    RowBox[{"Calcular", " ", "media", " ", "y", " ", "varianza"}], "*)"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"media", "=", 
     RowBox[{"Mean", "[", "trayectorias", "]"}]}], ";"}], "\n", 
   RowBox[{
    RowBox[{"varianza", "=", 
     RowBox[{"Variance", "[", "trayectorias", "]"}]}], ";"}], 
   "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{"(*", 
    RowBox[{"Mostrar", " ", "resultados"}], "*)"}], "\n", 
   RowBox[{
    RowBox[{"Print", "[", 
     RowBox[{"\"\<Media: \>\"", ",", "media"}], "]"}], ";"}], "\n", 
   RowBox[{
    RowBox[{"Print", "[", 
     RowBox[{"\"\<Varianza: \>\"", ",", "varianza"}], "]"}], ";"}], 
   "\n"}]}]], "Input",
 CellChangeTimes->{{3.9197937959799747`*^9, 3.91979380468618*^9}, {
  3.9197938688410034`*^9, 3.919793870090296*^9}},
 CellLabel->"In[49]:=",ExpressionUUID->"b78d872b-1daf-cd43-b6d0-05602cd8b88b"],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Media: \"\>", "\[InvisibleSpace]", 
   RowBox[{"{", 
    RowBox[{
     InterpretationBox[
      RowBox[{
       TagBox["InterpolatingFunction",
        "SummaryHead"], "[", 
       DynamicModuleBox[{Typeset`open$$ = False, Typeset`embedState$$ = 
        "Ready"}, 
        TemplateBox[{
          PaneSelectorBox[{False -> GridBox[{{
                PaneBox[
                 ButtonBox[
                  DynamicBox[
                   FEPrivate`FrontEndResource[
                   "FEBitmaps", "SummaryBoxOpener"]], 
                  ButtonFunction :> (Typeset`open$$ = True), Appearance -> 
                  None, BaseStyle -> {}, Evaluator -> Automatic, Method -> 
                  "Preemptive"], Alignment -> {Center, Center}, ImageSize -> 
                 Dynamic[{
                   Automatic, 
                    3.5 (CurrentValue["FontCapHeight"]/AbsoluteCurrentValue[
                    Magnification])}]], 
                GraphicsBox[{{{{}, {}, 
                    TagBox[{
                    Directive[
                    Opacity[1.], 
                    RGBColor[0.368417, 0.506779, 0.709798], 
                    AbsoluteThickness[1]], 
                    LineBox[CompressedData["
1:eJwVyn9Q03Ucx/EBDmFhIGOsAeJ+YJKGIj+mpvJ+gyIyEmPuMJznCSeSxI8g
OISFoMXPQ6kQEFnHQgITFcR1onCFmFQj/BHNYFroRFoIIptua2P79O2P1z3v
cffipWSLUx1pNNpGav+3VRiS+SBuEoJ2Jwulj42gbq6TJUmeQv2f95LTgk3Q
d6P3uG1+GuJdo2X5zWYoFTWKxtgvgMnaOrH/2r/QF3vV/eFzPYi7L6V++70F
DFURjV/1voJPvHMVt+utoOFmtR0SmyH2SuRH+2AB9o4cn00KsEJyTU94k2YB
qu5Cv2TIBinbG/KHJTaQMnray4NoeCA1q+HL6zaIuCpcYq5zQP10eNQs2w5H
amuFmVpHzPnh5LqCZDuU6DXFpncXIaq5jN42O9CNSe2PuukYFipd5//QDtqA
i1+MLDjjhlOn+arFBGr+0p+5c9AFB68o3hlZS+DETbc0tdIVRWOSw/G7CEhD
OQkvuK/h0ONGJ+8MAh1bk/zSy93QcHbF70OfEogryTz/9+gSrLvh05rTQKA/
Oi+xaIs7prOHbOUdBAabO1IqYzywePLRpFlJ4OnkJYbTrAfGKnznVg0QyP64
K7JSsRQva7QXslQEvmH77HXf4Ynj8sBdn/1GINLhqMPXNk9cOfNqvW6cgF+f
jKPtYmK97uRm+wSB1uonnisSvXD4nnK9apJAbcu25lwGCzPF4rtr/iEQqHKn
D/awMDhU92bbDAHarbyJgT3eaKxWdZfNEYigj+5QMdhYV30tcZWe+kub69SX
2RhitXwALwkcMe7LfHboDcwoEjBXGgnkdVQGOnlwUH59+na7iUBXYLxE9R0H
g02lW6bMBBLm3Nzlh30wRMyxMSwEWobDO7OX+qKCL6qYp7x7ds/9nT/5YuGF
g0E/WgnETDBzHxT6YaOh6nTOAoGpqY1z6dxlWFpYs81C+VloknWRehm6nFh9
oMBGQN6g1SmK/VEceO6Pcco1v9BjZvjL0S92WLPaTkAbyQ+Lur8cvSwqjwLK
+jSJw0gEF/dvHt3ZQ7k9wSv4aBMXj9nyCnWUiwQ8doWei5uaetgsQiCA9uT5
59E8lNkGojZQPlbCe13RwkO6aZPsfcpKV5Z/t4GH8HbMWA7lM8Ky/FvxfEx8
645jFeWWuKiykHN8FBnEXnLKRR8qyxRWPjpdNHM6KbusOTvCTBRg2Mv+8V7K
GdbtzpXnBShwrui8STnE6z0LIQIsXPvz/K+U/wN3BsfG
                    "]]}, Annotation[#, "Charting`Private`Tag#1"]& ]}}, {}}, {
                 DisplayFunction -> Identity, Ticks -> {Automatic, Automatic},
                   AxesOrigin -> {0, 1.430018627569736}, 
                  FrameTicks -> {{{}, {}}, {{}, {}}}, 
                  GridLines -> {None, None}, DisplayFunction -> Identity, 
                  PlotRangePadding -> {{
                    Scaled[0.1], 
                    Scaled[0.1]}, {
                    Scaled[0.1], 
                    Scaled[0.1]}}, PlotRangeClipping -> True, ImagePadding -> 
                  All, DisplayFunction -> Identity, AspectRatio -> 1, 
                  Axes -> {False, False}, AxesLabel -> {None, None}, 
                  AxesOrigin -> {0, 1.4300186275697329`}, DisplayFunction :> 
                  Identity, Frame -> {{True, True}, {True, True}}, 
                  FrameLabel -> {{None, None}, {None, None}}, FrameStyle -> 
                  Directive[
                    Opacity[0.5], 
                    Thickness[Tiny], 
                    RGBColor[0.368417, 0.506779, 0.709798]], 
                  FrameTicks -> {{None, None}, {None, None}}, 
                  GridLines -> {None, None}, GridLinesStyle -> Directive[
                    GrayLevel[0.5, 0.4]], ImageSize -> 
                  Dynamic[{
                    Automatic, 
                    3.5 (CurrentValue["FontCapHeight"]/AbsoluteCurrentValue[
                    Magnification])}], 
                  Method -> {
                   "DefaultBoundaryStyle" -> Automatic, 
                    "DefaultGraphicsInteraction" -> {
                    "Version" -> 1.2, "TrackMousePosition" -> {True, False}, 
                    "Effects" -> {
                    "Highlight" -> {"ratio" -> 2}, 
                    "HighlightPoint" -> {"ratio" -> 2}, 
                    "Droplines" -> {
                    "freeformCursorMode" -> True, 
                    "placement" -> {"x" -> "All", "y" -> "None"}}}}, 
                    "DefaultMeshStyle" -> AbsolutePointSize[6], 
                    "ScalingFunctions" -> None, 
                    "CoordinatesToolOptions" -> {"DisplayFunction" -> ({
                    (Identity[#]& )[
                    Part[#, 1]], 
                    (Identity[#]& )[
                    Part[#, 2]]}& ), "CopiedValueFunction" -> ({
                    (Identity[#]& )[
                    Part[#, 1]], 
                    (Identity[#]& )[
                    Part[#, 2]]}& )}}, 
                  PlotRange -> {{0., 10.}, {1.4300186275697329`, 
                   1.9999513096435904`}}, PlotRangeClipping -> True, 
                  PlotRangePadding -> {{
                    Scaled[0.1], 
                    Scaled[0.1]}, {
                    Scaled[0.1], 
                    Scaled[0.1]}}, Ticks -> {Automatic, Automatic}}], 
                GridBox[{{
                   RowBox[{
                    TagBox["\"Domain: \"", "SummaryItemAnnotation"], 
                    "\[InvisibleSpace]", 
                    TagBox[
                    RowBox[{"{", 
                    RowBox[{"{", 
                    RowBox[{"0.`", ",", "10.`"}], "}"}], "}"}], 
                    "SummaryItem"]}]}, {
                   RowBox[{
                    TagBox["\"Output: \"", "SummaryItemAnnotation"], 
                    "\[InvisibleSpace]", 
                    TagBox["\"scalar\"", "SummaryItem"]}]}}, 
                 GridBoxAlignment -> {
                  "Columns" -> {{Left}}, "Rows" -> {{Automatic}}}, AutoDelete -> 
                 False, GridBoxItemSize -> {
                  "Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}, 
                 GridBoxSpacings -> {
                  "Columns" -> {{2}}, "Rows" -> {{Automatic}}}, 
                 BaseStyle -> {
                  ShowStringCharacters -> False, NumberMarks -> False, 
                   PrintPrecision -> 3, ShowSyntaxStyles -> False}]}}, 
              GridBoxAlignment -> {"Columns" -> {{Left}}, "Rows" -> {{Top}}}, 
              AutoDelete -> False, 
              GridBoxItemSize -> {
               "Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}, 
              BaselinePosition -> {1, 1}], True -> GridBox[{{
                PaneBox[
                 ButtonBox[
                  DynamicBox[
                   FEPrivate`FrontEndResource[
                   "FEBitmaps", "SummaryBoxCloser"]], 
                  ButtonFunction :> (Typeset`open$$ = False), Appearance -> 
                  None, BaseStyle -> {}, Evaluator -> Automatic, Method -> 
                  "Preemptive"], Alignment -> {Center, Center}, ImageSize -> 
                 Dynamic[{
                   Automatic, 
                    3.5 (CurrentValue["FontCapHeight"]/AbsoluteCurrentValue[
                    Magnification])}]], 
                GraphicsBox[{{{{}, {}, 
                    TagBox[{
                    Directive[
                    Opacity[1.], 
                    RGBColor[0.368417, 0.506779, 0.709798], 
                    AbsoluteThickness[1]], 
                    LineBox[CompressedData["
1:eJwVyn9Q03Ucx/EBDmFhIGOsAeJ+YJKGIj+mpvJ+gyIyEmPuMJznCSeSxI8g
OISFoMXPQ6kQEFnHQgITFcR1onCFmFQj/BHNYFroRFoIIptua2P79O2P1z3v
cffipWSLUx1pNNpGav+3VRiS+SBuEoJ2Jwulj42gbq6TJUmeQv2f95LTgk3Q
d6P3uG1+GuJdo2X5zWYoFTWKxtgvgMnaOrH/2r/QF3vV/eFzPYi7L6V++70F
DFURjV/1voJPvHMVt+utoOFmtR0SmyH2SuRH+2AB9o4cn00KsEJyTU94k2YB
qu5Cv2TIBinbG/KHJTaQMnray4NoeCA1q+HL6zaIuCpcYq5zQP10eNQs2w5H
amuFmVpHzPnh5LqCZDuU6DXFpncXIaq5jN42O9CNSe2PuukYFipd5//QDtqA
i1+MLDjjhlOn+arFBGr+0p+5c9AFB68o3hlZS+DETbc0tdIVRWOSw/G7CEhD
OQkvuK/h0ONGJ+8MAh1bk/zSy93QcHbF70OfEogryTz/9+gSrLvh05rTQKA/
Oi+xaIs7prOHbOUdBAabO1IqYzywePLRpFlJ4OnkJYbTrAfGKnznVg0QyP64
K7JSsRQva7QXslQEvmH77HXf4Ynj8sBdn/1GINLhqMPXNk9cOfNqvW6cgF+f
jKPtYmK97uRm+wSB1uonnisSvXD4nnK9apJAbcu25lwGCzPF4rtr/iEQqHKn
D/awMDhU92bbDAHarbyJgT3eaKxWdZfNEYigj+5QMdhYV30tcZWe+kub69SX
2RhitXwALwkcMe7LfHboDcwoEjBXGgnkdVQGOnlwUH59+na7iUBXYLxE9R0H
g02lW6bMBBLm3Nzlh30wRMyxMSwEWobDO7OX+qKCL6qYp7x7ds/9nT/5YuGF
g0E/WgnETDBzHxT6YaOh6nTOAoGpqY1z6dxlWFpYs81C+VloknWRehm6nFh9
oMBGQN6g1SmK/VEceO6Pcco1v9BjZvjL0S92WLPaTkAbyQ+Lur8cvSwqjwLK
+jSJw0gEF/dvHt3ZQ7k9wSv4aBMXj9nyCnWUiwQ8doWei5uaetgsQiCA9uT5
59E8lNkGojZQPlbCe13RwkO6aZPsfcpKV5Z/t4GH8HbMWA7lM8Ky/FvxfEx8
645jFeWWuKiykHN8FBnEXnLKRR8qyxRWPjpdNHM6KbusOTvCTBRg2Mv+8V7K
GdbtzpXnBShwrui8STnE6z0LIQIsXPvz/K+U/wN3BsfG
                    "]]}, Annotation[#, "Charting`Private`Tag#1"]& ]}}, {}}, {
                 DisplayFunction -> Identity, Ticks -> {Automatic, Automatic},
                   AxesOrigin -> {0, 1.430018627569736}, 
                  FrameTicks -> {{{}, {}}, {{}, {}}}, 
                  GridLines -> {None, None}, DisplayFunction -> Identity, 
                  PlotRangePadding -> {{
                    Scaled[0.1], 
                    Scaled[0.1]}, {
                    Scaled[0.1], 
                    Scaled[0.1]}}, PlotRangeClipping -> True, ImagePadding -> 
                  All, DisplayFunction -> Identity, AspectRatio -> 1, 
                  Axes -> {False, False}, AxesLabel -> {None, None}, 
                  AxesOrigin -> {0, 1.4300186275697329`}, DisplayFunction :> 
                  Identity, Frame -> {{True, True}, {True, True}}, 
                  FrameLabel -> {{None, None}, {None, None}}, FrameStyle -> 
                  Directive[
                    Opacity[0.5], 
                    Thickness[Tiny], 
                    RGBColor[0.368417, 0.506779, 0.709798]], 
                  FrameTicks -> {{None, None}, {None, None}}, 
                  GridLines -> {None, None}, GridLinesStyle -> Directive[
                    GrayLevel[0.5, 0.4]], ImageSize -> 
                  Dynamic[{
                    Automatic, 
                    3.5 (CurrentValue["FontCapHeight"]/AbsoluteCurrentValue[
                    Magnification])}], 
                  Method -> {
                   "DefaultBoundaryStyle" -> Automatic, 
                    "DefaultGraphicsInteraction" -> {
                    "Version" -> 1.2, "TrackMousePosition" -> {True, False}, 
                    "Effects" -> {
                    "Highlight" -> {"ratio" -> 2}, 
                    "HighlightPoint" -> {"ratio" -> 2}, 
                    "Droplines" -> {
                    "freeformCursorMode" -> True, 
                    "placement" -> {"x" -> "All", "y" -> "None"}}}}, 
                    "DefaultMeshStyle" -> AbsolutePointSize[6], 
                    "ScalingFunctions" -> None, 
                    "CoordinatesToolOptions" -> {"DisplayFunction" -> ({
                    (Identity[#]& )[
                    Part[#, 1]], 
                    (Identity[#]& )[
                    Part[#, 2]]}& ), "CopiedValueFunction" -> ({
                    (Identity[#]& )[
                    Part[#, 1]], 
                    (Identity[#]& )[
                    Part[#, 2]]}& )}}, 
                  PlotRange -> {{0., 10.}, {1.4300186275697329`, 
                   1.9999513096435904`}}, PlotRangeClipping -> True, 
                  PlotRangePadding -> {{
                    Scaled[0.1], 
                    Scaled[0.1]}, {
                    Scaled[0.1], 
                    Scaled[0.1]}}, Ticks -> {Automatic, Automatic}}], 
                GridBox[{{
                   RowBox[{
                    TagBox["\"Domain: \"", "SummaryItemAnnotation"], 
                    "\[InvisibleSpace]", 
                    TagBox[
                    RowBox[{"{", 
                    RowBox[{"{", 
                    RowBox[{"0.`", ",", "10.`"}], "}"}], "}"}], 
                    "SummaryItem"]}]}, {
                   RowBox[{
                    TagBox["\"Output: \"", "SummaryItemAnnotation"], 
                    "\[InvisibleSpace]", 
                    TagBox["\"scalar\"", "SummaryItem"]}]}, {
                   RowBox[{
                    TagBox["\"Order: \"", "SummaryItemAnnotation"], 
                    "\[InvisibleSpace]", 
                    TagBox["3", "SummaryItem"]}]}, {
                   RowBox[{
                    TagBox["\"Method: \"", "SummaryItemAnnotation"], 
                    "\[InvisibleSpace]", 
                    TagBox["\"Hermite\"", "SummaryItem"]}]}, {
                   RowBox[{
                    TagBox["\"Periodic: \"", "SummaryItemAnnotation"], 
                    "\[InvisibleSpace]", 
                    TagBox["False", "SummaryItem"]}]}}, 
                 GridBoxAlignment -> {
                  "Columns" -> {{Left}}, "Rows" -> {{Automatic}}}, AutoDelete -> 
                 False, GridBoxItemSize -> {
                  "Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}, 
                 GridBoxSpacings -> {
                  "Columns" -> {{2}}, "Rows" -> {{Automatic}}}, 
                 BaseStyle -> {
                  ShowStringCharacters -> False, NumberMarks -> False, 
                   PrintPrecision -> 3, ShowSyntaxStyles -> False}]}}, 
              GridBoxAlignment -> {"Columns" -> {{Left}}, "Rows" -> {{Top}}}, 
              AutoDelete -> False, 
              GridBoxItemSize -> {
               "Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}, 
              BaselinePosition -> {1, 1}]}, 
           Dynamic[Typeset`open$$], ImageSize -> Automatic]},
         "SummaryPanel"],
        DynamicModuleValues:>{}], "]"}],
      InterpolatingFunction[{{0., 10.}}, {
       5, 7, 1, {69}, {4}, 0, 0, 0, 0, Automatic, {}, {}, 
        False}, CompressedData["
1:eJxTTMoPSmViYGBgBGJXBgRgTJr6+fkZdXsIbW5/SYzru3NEsf2h7Nztd2Y2
2UufCS2e3t5tv/Kjog3Xz9n2i+p84lXEl9oXsHQsOGy4xl7+6MMCw2Ub7d94
yW66cGCH/a4LEZ/zb++3d9H2msVefdhe+91zq3CO4/ZCG1tvL5t6yn7Ohduh
b+9dst9w4oste/EN+9U5T9xKXt+xD+Z4tfzDxgf2gbcSddTfPbL3W3NzQ6zW
U3ufukDTKWnP7b0CTu48teilveuXHYfMpd/ZK/hcSb3k+dF+ZWRd2BL5z/ZG
6RruZV+/2O8uuWTucfqbvXNTjYbUwh/2Z/rVJN+U/bIPnXuBc5/PH/t7q6p+
9Sv9s8+9fczhSjyDQ3ZiQEvCOkaHjOc3j7/5w+SQlpvMXenN4pDy+Y0f6yxW
h6TKskkTX7A5JPz/f1XWnMMhomdy/vYLnA6BkuqcgVncDl7Ldi16xczr4Gzs
Z9Myl89B7sGHV33SAg6zlu3aHv1CwEE8t6VFY6ugw2Rjv8CvjUIOAr/E5Q75
CTv0HHgIVCfiwNm+GqhOxIH37TKelquiDoLBixLXHBJzENs5d9uV9eIOUvIz
uf/OkXCQa52SoNol6aD0un+rb7mUg3pgN1dZirSDTGU+kJZxEFwYDKRlHdhO
mgNpOYffH6SBtLxDBQ8Dq7WRgkORwicW600KDrkmT1isjRQdVF1eK0dfUnS4
F/zJqbpIyWFa8s/E2ULKDqc3/tng26nsAEkdKg4AW6nxpg==
       "], {
       Developer`PackedArrayForm, {0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 
        24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50, 52, 54, 56, 
        58, 60, 62, 64, 66, 68, 70, 72, 74, 76, 78, 80, 82, 84, 86, 88, 90, 
        92, 94, 96, 98, 100, 102, 104, 106, 108, 110, 112, 114, 116, 118, 120,
         122, 124, 126, 128, 130, 132, 134, 136, 138}, CompressedData["
1:eJwd0GlQU1cYxvGEIgyogArSQlksjECtmtbYokgelQ5oBLcShMo2VFksFBNb
jWglyCoiojDilE00EJY2LENSFzBRrApEiogC2ilJkFFbQ3IpBAwktxc/nA+/
+b/nvDNnRVzq3oN0Go2WTx2h4MCrI2INxLKQRfe9CbgksTYuatYgYZNK/c6L
gNDONU3YokFFInF3NeW18uGpoUENWArb7GX2BCwDz4ypjRqE32Ces6ETKC8t
5N/1GMeEY2un67gOll8nh64rG0e/IoMT36pD+IOvCJOdFo49bbmCYzrUL7Gx
WpWthQU7/cQhfx2I2F85HdNaFDgzOCvMdeDcq89zSNYhbo+jdsBLixbPts83
DutQqQ0vPxtC7bnq6h0XQCDPM7PJ77gGOan3v73aQCBfe4h9q+EtbAeue9ra
TqDbNkeqUv2LgU69ZMb3P4SPVhfZFL6BplG8yJw9ibTKg6PT37+Ci7A3O8h+
CkObmTOHB8dwtZH2y1jfFAyq2W5m9EsciJEYuwR6xOblLkg2qXGh5AW/z3Ma
1vZFNjFSFbyCfqTd75hGrSSRPJKlhKm8WZIUPANRgconovdvFOaq8/kh77A7
iaV06X4OUV/C0Q9WGzAZZcGa2DGI11npg5d6DfBW77wo/GMA3BMyuSh6Ftz6
aTI2ux+PPEq0ziOziOyZ2lSU2QfStypTuGeO+pfF6he8R/jpi5eNnb/PYbiE
X6Xe0o3jae3tj5caMbPyvCMv6QG4PW9v0g8Ykern7zUxcg8/NGjGf3tohPu1
CEHRzTuwYKRYrnE1gTzlHOHmchuG0mvf9cRTjg6t3F58A8/3G3yOCU1g8N39
z12XoNvUWjw4ZMJIWUptDq8VGUsSfb9ZQMItePrOZU4TSkbihU6rSMh4yqDG
8gY4tUUtad1DQs5amNHRVYMWm5VCNpeE0mrbxsZn1ehwaDLzOUt1RVe8fH0F
yFG/ybArJDaHZ4kC/imF2+H9duIWqr8+veZkSTFiP0oTKGVUXyp2UloWYe2j
be6CHhK0LGm/FvlYGJMRaf6URPozlunxz9mQbK2e7f2L6vnunNOXM7BLHmDN
GKVsRQxM/XkS3A2fLkt6TYK8JXXTXDqGwMDcM9s1lPvNy45KedDVz415EdT8
eIGS2ZyCU96NWRsmKcfVsPQFCagNq7xep6fM6xIViOMwyVQ4lM5QnutcV3Mt
Cgl1C+FmoKy0ltIy92GNfhcnbJYE/SWLWVe7F+yPQ2Oi56jum76TtjgE+yoS
6Z8Zqf2vnvqNVwWClrfcS0GZRqv27qzcguitHsptJsqx0jY91w8rK3hmDfMu
cqE9iVmPyOaYuql5911iHq1hgN1+/iKDnL8vMxNpfJDcbgxKnPeVklBC/wk6
WjItLlCmC2yHvrztDN962RPxfN/MsNq9wx7r976pkr03f7nbh3YQyR8Sivfv
BRusc63wPxEANJk=
        "]}, {Automatic}],
      Editable->False,
      SelectWithContents->True,
      Selectable->False], "[", "t", "]"}], "}"}]}],
  SequenceForm["Media: ", {
    InterpolatingFunction[{{0., 10.}}, {
     5, 7, 1, {69}, {4}, 0, 0, 0, 0, Automatic, {}, {}, 
      False}, CompressedData["
1:eJxTTMoPSmViYGBgBGJXBgRgTJr6+fkZdXsIbW5/SYzru3NEsf2h7Nztd2Y2
2UufCS2e3t5tv/Kjog3Xz9n2i+p84lXEl9oXsHQsOGy4xl7+6MMCw2Ub7d94
yW66cGCH/a4LEZ/zb++3d9H2msVefdhe+91zq3CO4/ZCG1tvL5t6yn7Ohduh
b+9dst9w4oste/EN+9U5T9xKXt+xD+Z4tfzDxgf2gbcSddTfPbL3W3NzQ6zW
U3ufukDTKWnP7b0CTu48teilveuXHYfMpd/ZK/hcSb3k+dF+ZWRd2BL5z/ZG
6RruZV+/2O8uuWTucfqbvXNTjYbUwh/2Z/rVJN+U/bIPnXuBc5/PH/t7q6p+
9Sv9s8+9fczhSjyDQ3ZiQEvCOkaHjOc3j7/5w+SQlpvMXenN4pDy+Y0f6yxW
h6TKskkTX7A5JPz/f1XWnMMhomdy/vYLnA6BkuqcgVncDl7Ldi16xczr4Gzs
Z9Myl89B7sGHV33SAg6zlu3aHv1CwEE8t6VFY6ugw2Rjv8CvjUIOAr/E5Q75
CTv0HHgIVCfiwNm+GqhOxIH37TKelquiDoLBixLXHBJzENs5d9uV9eIOUvIz
uf/OkXCQa52SoNol6aD0un+rb7mUg3pgN1dZirSDTGU+kJZxEFwYDKRlHdhO
mgNpOYffH6SBtLxDBQ8Dq7WRgkORwicW600KDrkmT1isjRQdVF1eK0dfUnS4
F/zJqbpIyWFa8s/E2ULKDqc3/tng26nsAEkdKg4AW6nxpg==
     "], {
     Developer`PackedArrayForm, {0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 
      24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50, 52, 54, 56, 58, 
      60, 62, 64, 66, 68, 70, 72, 74, 76, 78, 80, 82, 84, 86, 88, 90, 92, 94, 
      96, 98, 100, 102, 104, 106, 108, 110, 112, 114, 116, 118, 120, 122, 124,
       126, 128, 130, 132, 134, 136, 138}, CompressedData["
1:eJwd0GlQU1cYxvGEIgyogArSQlksjECtmtbYokgelQ5oBLcShMo2VFksFBNb
jWglyCoiojDilE00EJY2LENSFzBRrApEiogC2ilJkFFbQ3IpBAwktxc/nA+/
+b/nvDNnRVzq3oN0Go2WTx2h4MCrI2INxLKQRfe9CbgksTYuatYgYZNK/c6L
gNDONU3YokFFInF3NeW18uGpoUENWArb7GX2BCwDz4ypjRqE32Ces6ETKC8t
5N/1GMeEY2un67gOll8nh64rG0e/IoMT36pD+IOvCJOdFo49bbmCYzrUL7Gx
WpWthQU7/cQhfx2I2F85HdNaFDgzOCvMdeDcq89zSNYhbo+jdsBLixbPts83
DutQqQ0vPxtC7bnq6h0XQCDPM7PJ77gGOan3v73aQCBfe4h9q+EtbAeue9ra
TqDbNkeqUv2LgU69ZMb3P4SPVhfZFL6BplG8yJw9ibTKg6PT37+Ci7A3O8h+
CkObmTOHB8dwtZH2y1jfFAyq2W5m9EsciJEYuwR6xOblLkg2qXGh5AW/z3Ma
1vZFNjFSFbyCfqTd75hGrSSRPJKlhKm8WZIUPANRgconovdvFOaq8/kh77A7
iaV06X4OUV/C0Q9WGzAZZcGa2DGI11npg5d6DfBW77wo/GMA3BMyuSh6Ftz6
aTI2ux+PPEq0ziOziOyZ2lSU2QfStypTuGeO+pfF6he8R/jpi5eNnb/PYbiE
X6Xe0o3jae3tj5caMbPyvCMv6QG4PW9v0g8Ykern7zUxcg8/NGjGf3tohPu1
CEHRzTuwYKRYrnE1gTzlHOHmchuG0mvf9cRTjg6t3F58A8/3G3yOCU1g8N39
z12XoNvUWjw4ZMJIWUptDq8VGUsSfb9ZQMItePrOZU4TSkbihU6rSMh4yqDG
8gY4tUUtad1DQs5amNHRVYMWm5VCNpeE0mrbxsZn1ehwaDLzOUt1RVe8fH0F
yFG/ybArJDaHZ4kC/imF2+H9duIWqr8+veZkSTFiP0oTKGVUXyp2UloWYe2j
be6CHhK0LGm/FvlYGJMRaf6URPozlunxz9mQbK2e7f2L6vnunNOXM7BLHmDN
GKVsRQxM/XkS3A2fLkt6TYK8JXXTXDqGwMDcM9s1lPvNy45KedDVz415EdT8
eIGS2ZyCU96NWRsmKcfVsPQFCagNq7xep6fM6xIViOMwyVQ4lM5QnutcV3Mt
Cgl1C+FmoKy0ltIy92GNfhcnbJYE/SWLWVe7F+yPQ2Oi56jum76TtjgE+yoS
6Z8Zqf2vnvqNVwWClrfcS0GZRqv27qzcguitHsptJsqx0jY91w8rK3hmDfMu
cqE9iVmPyOaYuql5911iHq1hgN1+/iKDnL8vMxNpfJDcbgxKnPeVklBC/wk6
WjItLlCmC2yHvrztDN962RPxfN/MsNq9wx7r976pkr03f7nbh3YQyR8Sivfv
BRusc63wPxEANJk=
      "]}, {Automatic}][$CellContext`t]}],
  Editable->False]], "Print",
 CellChangeTimes->{{3.91979383030015*^9, 3.9197938935670376`*^9}},
 CellLabel->
  "Durante la evaluaci\[OAcute]n de \
In[49]:=",ExpressionUUID->"dc427e33-f10b-3545-93e4-4d3b51d98f52"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Varianza: \"\>", "\[InvisibleSpace]", 
   RowBox[{"{", "0", "}"}]}],
  SequenceForm["Varianza: ", {0}],
  Editable->False]], "Print",
 CellChangeTimes->{{3.91979383030015*^9, 3.919793893580616*^9}},
 CellLabel->
  "Durante la evaluaci\[OAcute]n de \
In[49]:=",ExpressionUUID->"e22f5189-ec0c-7544-bd77-89fa04a7cdc2"]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1024.5, 543.75},
WindowMargins->{{-6, Automatic}, {Automatic, -6}},
FrontEndVersion->"14.0 para Microsoft Windows (64-bit) (December 12, 2023)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"b39d63f3-e35e-ac43-819a-402deba21661"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 3407, 96, 545, "Input",ExpressionUUID->"b78d872b-1daf-cd43-b6d0-05602cd8b88b"],
Cell[CellGroupData[{
Cell[4012, 122, 19811, 374, 51, "Print",ExpressionUUID->"dc427e33-f10b-3545-93e4-4d3b51d98f52"],
Cell[23826, 498, 364, 9, 22, "Print",ExpressionUUID->"e22f5189-ec0c-7544-bd77-89fa04a7cdc2"]
}, Open  ]]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

